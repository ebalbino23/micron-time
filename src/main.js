import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.config.errorHandler = (err) => {
  console.log('Exception: ', err);
};

new Vue({
  render: h => h(App),
}).$mount('#app');
